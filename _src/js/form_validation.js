// var flatpickr = require("flatpickr");
import flatpickr from "flatpickr"
// import { Japan } from "flatpickr/dist/l10n/ja.js"
const Japan = require("flatpickr/dist/l10n/ja.js").default.ja;
export function formvalidation() {
    var $form = $("#js-valid");
    var flg_error = 1;
    var flg_submit = false;

    flatpickr.localize(Japan);

    flatpickr(".datepick", {
        "dateFormat": "Y-m-d",
        "minDate": "today"
    });

    // $("#js-valid").on("submit", function () {
    $("#js-valid .btn-submit a").on("click", function () {
        var arr_check = [];
        $(".required-check").each(function (i) {
            // console.log("index = " + i, "  val = " + $(this).val());
            if ($(this).val() === "") {
                flg_error = 1;
                arr_check.push(flg_error);

                if ($(this).hasClass("flatpickr-mobile")) {
                    if (i === 7) {
                        $(".required").eq(6).addClass("error");
                        $(".err").eq(6).show();
                    } else if (i === 9) {
                        $(".required").eq(7).addClass("error");
                        $(".err").eq(7).show();
                    }else if (i === 11) {
                        $(".required").eq(8).addClass("error");
                        $(".err").eq(8).show();
                    }

                } else {
                    $(".required").eq(i).addClass("error");
                    $(".err").eq(i).show();
                }
            } else {
                flg_error = 0;
                arr_check.push(flg_error);
                $(".required").eq(i).removeClass("error");
                $(".err").eq(i).hide();

            }
        });
        if ($.inArray(1, arr_check) >= 0) {
            $("#js-message").show().css({
                "display": "block"
            });
            $('html,body').animate({
                scrollTop: ($("#js-message").offset().top - 90)
            }, {
                duration: 800,
                easing: "swing"
            });
            return false;
        } else {
            reservation.submit();
        }
        // console.log($.inArray(1, arr_check));
        // for (var i = 0; i < arr_check.length; i++) {
        //     if (arr_check[i] == true) {
        //         flg_submit = false;
        //     }
        // }
        if (flg_error) {
            $("#js-message").show().css({
                "display": "block"
            });
            $('html,body').animate({scrollTop: ($("#js-message").offset().top - 90)}, {duration: 800,easing: "swing"});
            return false;
        }
    });
}