// import "@babel/polyfill";
// import { slider } from './swiper_setting';
// import { loadGoogleMapsApi } from './map';
// import { bg_slide } from './bgslider';
// import { formvalidation } from './form_validation';
// import { getInfo } from './top_info';

(function ($) {

    // bg_slide();

    var $win = $(window);
    var timer = 0;
    var $grid = $('.grade-list ul');

    /**
     * windowイベント
     */
    $win.on({
        "load": function () {
            // listFillIn();
        },
        "scroll": function () {
            // scrollTop = $win.scrollTop();
            // winEnd = scrollTop + winH;
        },
        "resize": function () {
            if (timer > 0) {
                clearTimeout(timer);
            }

            timer = setTimeout(function () {
                // listFillIn();
            }, 100);
        }
    });


    var pdfArr = [];
    var mp3Arr = [];

    $("a").each(function(i){
        var hrefdata = $(this).attr("href");

        if (hrefdata.match(/.pdf/)) {
            pdfArr.push(hrefdata);
        }
        if (hrefdata.match(/.mp3/)) {
            mp3Arr.push(hrefdata);
        }
        if(hrefdata === ""){
            $(this).remove();
        }
    });

    for (var i = 0; i < pdfArr.length; i++) {
        // urlExists(pdfArr[i]);
        urlExists(pdfArr[i], function (exists) {
            console.log('"%s" exists?', pdfArr[i], exists);
        });
    }
    for (var i = 0; i < mp3Arr.length; i++) {
        // urlExists(mp3Arr[i]);
        urlExists(mp3Arr[i], function (exists) {
            console.log('"%s" exists?', mp3Arr[i], exists);
        });
    }

    function urlExists(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                callback(xhr.status < 400);
            }
        };
        xhr.open('HEAD', url);
        xhr.send();
    }



    // function listFillIn(){
    //     var emptyCells = []
    //         , i
    //         ;

    //     // 子パネル (ul.cell) の数だけ空の子パネル (ul.cell.is-empty) を追加する。
    //     for (i = 0; i < $grid.find('li').length; i++) {
    //         emptyCells.push($('<li>', { class: 'is-empty' }));
    //     }

    //     $grid.append(emptyCells);
    // }
})(jQuery);