"use strict";
const gulp            = require('gulp');
const webpackStream = require("webpack-stream");
const webpack       = require("webpack");
const webpackConfig = require("./webpack.config");

function webpackScript() {
    return webpackStream(webpackConfig, webpack)
        .pipe(gulp.dest("dist"));
}
exports.webpackScript = webpackScript;